import java.util.Collections;

class HoughComparator implements java.util.Comparator<Integer> {
  int[] accumulator;
  public HoughComparator(int[] accumulator) {
    this.accumulator = accumulator;
  }
@Override
   public int compare(Integer l1, Integer l2) {
    if (accumulator[l1] > accumulator[l2]|| (accumulator[l1] == accumulator[l2] && l1 < l2))
      return -1;
    return 1;
  }
}
class Hough {
    float discretizationStepsPhi = 0.06f; 
    float discretizationStepsR = 2.5f; 
    int sizeRegion=10;
    int minVotes=100; 
    
    // dimensions of the accumulator
    int phiDim = (int) (Math.PI / discretizationStepsPhi +1);
    //The max radius is the image diagonal, but it can be also negative
    int rDim;
    int[] accumulator;
 
    // tabs for pre-compute of sin and cos values
    float[] tabSin = new float[phiDim];
    float[] tabCos = new float[phiDim];
    float ang = 0;
    float inverseR = 1.f / discretizationStepsR;
    
    Hough(PImage image){

       rDim = (int) ((sqrt(image.width*image.width + image.height*image.height) * 2) / discretizationStepsR +1);
       accumulator = new int[phiDim * rDim]; 
       
       for (int accPhi = 0; accPhi < phiDim; ang += discretizationStepsPhi, accPhi++) {
          // we can also pre-multiply by (1/discretizationStepsR) since we need it in the Hough loop
          tabSin[accPhi] = (float) (Math.sin(ang) * inverseR);
          tabCos[accPhi] = (float) (Math.cos(ang) * inverseR);
        }
    }


  
    List<PVector> hough(PImage edgeImg,int nLines) {
      
      for (int y = 0; y < edgeImg.height; y++) {
          for (int x = 0; x < edgeImg.width; x++) {
          
          if (brightness(edgeImg.pixels[y * edgeImg.width + x]) != 0) {
            for(int phi = 0; phi < phiDim; phi++){
              //double ar = (x*Math.cos(phi*discretizationStepsPhi) + y*Math.sin(phi*discretizationStepsPhi));
              double ar = (x*tabCos[phi] + y*tabSin[phi]); //CEST FAUX
              //int r = (int)(ar/discretizationStepsR);
              int r=(int)ar;
              accumulator[phi * rDim + r + rDim/2 ] += 1;
            }
            }
          }
        }
      ArrayList<PVector> lines=new ArrayList<PVector>();
      ArrayList<Integer> bestCandidates=new ArrayList<Integer>();
      
      for (int idx = 0; idx < accumulator.length; idx++) {
        if (accumulator[idx] > minVotes) {
          int max = 0;
          for(int a = idx-sizeRegion/2; a < idx + sizeRegion/2; ++a){
            for(int b = -sizeRegion/2; b < sizeRegion/2; ++b){
              int nInd = a + b*rDim;
              if(nInd % rDim< rDim -sizeRegion && nInd % rDim > sizeRegion && nInd >= 0 && nInd < accumulator.length){
                   if(accumulator[nInd] > max){
                      max = accumulator[nInd];
                  }
              }
            }
          }
          if(accumulator[idx] >= max){
            bestCandidates.add(idx);
          } 
        }
      }
      Collections.sort(bestCandidates, new HoughComparator(accumulator));
      //select best lines
      for(int k=0;k<nLines;k++){
        if(k<bestCandidates.size()){
          int idx=bestCandidates.get(k);
          // first, compute back the (r, phi) polar coordinates:
          int accPhi = (int) (idx / (rDim));
          int accR = idx - (accPhi) * (rDim);
          float r = (accR - (rDim) * 0.5f) * discretizationStepsR;
          float phi = accPhi * discretizationStepsPhi;
          lines.add(new PVector(r,phi));
        }
      }
     return lines;
    }
    
    void plot(List<PVector> lines, PImage edgeImg){
      for (int idx = 0; idx < lines.size(); idx++) {
        PVector line=lines.get(idx);
        float r = line.x;
        float phi = line.y;
        int x0 = 0;
        int y0 = (int) (r / sin(phi));
        int x1 = (int) (r / cos(phi));
        int y1 = 0;
        int x2 = edgeImg.width;
        int y2 = (int) (-cos(phi) / sin(phi) * x2 + r / sin(phi));
        int y3 = edgeImg.width;
        int x3 = (int) (-(y3 - r / sin(phi)) * (sin(phi) / cos(phi)));
        
        // Finally, plot the lines
        stroke(204,102,0);
        if (y0 > 0) {
          if (x1 > 0)
            line(x0, y0, x1, y1);
          else if (y2 > 0)
            line(x0, y0, x2, y2);
          else
            line(x0, y0, x3, y3);
        }else {
          if (x1 > 0) {
            if (y2 > 0)
              line(x1, y1, x2, y2);
            else
              line(x1, y1, x3, y3);
          }else{
            line(x2, y2, x3, y3);
          }
        }
      }
    }}