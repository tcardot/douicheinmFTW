import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.NavigableSet;
import java.util.Set;
import java.util.Iterator;
int currentLabel = 0;
List<TreeSet<Integer>> labelsEquivalences;
PImage findConnectedComponents(PImage input, boolean onlyBiggest) {

   currentLabel=1;
  // First pass: label the pixels and store labels' equivalences
  int [] labels= new int [input.width*input.height];
  labelsEquivalences= new ArrayList<TreeSet<Integer>>();
  
  for(int i = 0; i < input.width*input.height; ++i){
    labels[i] = 0;
  }
  input.loadPixels();
  for(int i = 0; i < input.width*input.height; ++i){
    if(input.pixels[i] != color(0,0,0)){
       labelPixel(input,i,labels);
       
    }
   
  }
  
  // TODO!
  // Second pass: re-label the pixels by their equivalent class
  // if onlyBiggest==true, count the number of pixels for each label
  // TODO!
  int [] nbforLabels = new int[labelsEquivalences.size()];
  for(int i = 0; i < input.width*input.height; ++ i){
      if(labels[i] != 0){
           labels[i] = labelsEquivalences.get(labels[i]-1).first();
       // if(onlyBiggest){
          ++nbforLabels[labels[i]-1];
       // }
      }

  }
  
  // Finally,
  // if onlyBiggest==false, output an image with each blob colored in one uniform color
  // if onlyBiggest==true, output an image with the biggest blob colored in white and the others in black
  // TODO!
    PImage result = createImage(input.width,input.height,RGB);
    result.loadPixels();
    int col[] = new int[labelsEquivalences.size()];
    for(int j = 0; j< col.length; j++){
      col[j] = /*color(255);*/color(random(255),random(255),random(255));
    }
    
    if(!onlyBiggest){  //if we want multiple blobs
      for(int i = 0; i < input.width*input.height; i++){
        if(labels[i] != 0){
          result.pixels[i] = col[labels[i]-1];//on applique la couleur du blob
        }else{
          result.pixels[i] = input.pixels[i];//on laisse la couleur originale
        }
      }
    }else{
       //index of label that contains the most pixels
      int max = 0; int maxLabel = -1; 
      for(int i = 0; i < nbforLabels.length; i++){
         if(nbforLabels[i] > max){
           max =  nbforLabels[i];
           maxLabel = i;
         }
      }
      //color only the blob that have the most pixels
      for(int i = 0; i < input.width *input.height; i++){
        if(labels[i]-1  == maxLabel) result.pixels[i] = color(255);//modified here, -1 since the start label is 1, see nbforLabels
        else result.pixels[i] = color(0);
      }
    }
    
    result.updatePixels();
    return result;
  
}
void labelPixel(PImage img, int index , int[] labels) {
  int[] neighbours = new int[4];
  int n1 = index - img.width -1;
  int n2 = index -img.width;
  int n3 = index -img.width +1;
  int n4 = index -1;

  //list labels of neighbours
  if (index%img.width != 0 && n1 >= 0)  neighbours[0] = labels[n1];  
  else neighbours[0] = 0;
  if (n2 >= 0) neighbours[1] = labels[n2] ;  
  else neighbours[1] = 0;
  if (n3 > 0 && index%img.width != img.width -1) neighbours[2] = labels[n3] ;  
  else neighbours[2] = 0;
  if (index%img.width != 0 && n4 >= 0 ) neighbours[3] = labels[n4] ;  
  else neighbours[3] = 0;

  // attribute label to index 
  int minLabel = Integer.MAX_VALUE;
  for (int i = 0; i < 4; ++i) {
    if (neighbours[i] <= minLabel && neighbours[i] > 0) {
      minLabel = neighbours[i];
    }
  }
  //create new label if if isolated
  if(minLabel == Integer.MAX_VALUE){
    labels[index] = 0;
 
    labels[index] = currentLabel;
    TreeSet<Integer> tree = new TreeSet<Integer>();
    tree.add(currentLabel);
    labelsEquivalences.add(tree);
    //labelsEquivalences.get(currentLabel-1).add(currentLabel);
    ++currentLabel;
// println(currentLabel);
    
  }else{
    labels[index] = minLabel;
  }
  //storing equivalences
  for (int i = 0; i < 4; ++i) {
    if (neighbours[i] != 0 && neighbours[i] != labels[index]) {
     //println(labelsEquivalences.get(neighbours[i]-1).size());
     labelsEquivalences.get(neighbours[i]-1).add(labels[index]);
     //labelsEquivalences.get(labels[index]
     TreeSet<Integer> tree = labelsEquivalences.get(neighbours[i]-1);
     tree.add(labels[index]);
     Iterator<Integer> it = labelsEquivalences.get(neighbours[i]-1).iterator();
     //labelsEquivalences.get(neighbours[i]-1).add(labels[index]);

      while(it.hasNext()){
          labelsEquivalences.get(it.next() -1 ).add(labels[index]);
      }
      it = labelsEquivalences.get(labels[index]-1).iterator();
      while(it.hasNext()){
        tree.add(it.next());
      }
    }
  }
}