import processing.video.*;

Capture cam;

PImage nim;
PImage img;
PImage imgt;
PImage imgSc;
PImage image_hough;

HScrollbar hueBar1;
HScrollbar hueBar2;
HScrollbar satBar1;
HScrollbar satBar2;
HScrollbar briBar1;
HScrollbar briBar2;

void settings() {
  size(1800, 700);
}
void keyPressed() {
  if (key==CODED) {
    if (keyCode==SHIFT) {
      nim.save("fotomarco.png");
    }
  }
}
void setup() {
    String[] cameras = Capture.list();
    if (cameras.length == 0) {
      println("There are no cameras available for capture.");
      exit();
    } else {
      println("Available cameras:");
      for (int i = 0; i < cameras.length; i++) {
        println(cameras[i]);
      }
    //select your predefined resolution from the list:
    cam = new Capture(this, cameras[0]);
    
  //If you're using gstreamer1.0 (Ubuntu 16.10 and later),
  //select your resolution manually instead:
  //cam = new Capture(this, 640, 480, cameras[0]);
    cam.start();
  }


  img = loadImage("board1.jpg");
  img.resize(400,300);
  //imgTh = loadImage("board1Thresholded.bmp");
  image_hough = loadImage("hough_test.bmp");
  hueBar1 = new HScrollbar(700,400 ,500, 20);
  hueBar2 = new HScrollbar(700,440,500,20);
  satBar1 = new HScrollbar(700, 480, 500, 20);
  satBar2 = new HScrollbar(700,520,500,20);
  briBar1 = new HScrollbar(700, 560, 500, 20);
  briBar2 = new HScrollbar(700,600,500,20);
 // PImage result = createImage(img.width, img.height, RGB);
 // result.loadPixels();
  nim = findConnectedComponents(loadImage("hough_test.bmp"),false);
  image(img,0,0);
}
 
void draw() {
  
  if (cam.available() == true) {
      cam.read();
    }
    
    image(img,0,0);
   // img = cam.get();
    //image(img, 0, 0);
  //image(image_hough, 0,0);
  imgt = thresholdHSB(img,(int)(hueBar1.getPos()*255),(int)(hueBar2.getPos()*255),(int)(satBar1.getPos()*255),(int)(satBar2.getPos()*255),(int)(briBar1.getPos()*255),(int)(briBar2.getPos()*255));
  image((threshold(scharr(imgt),100)),500,0);
  BlobDetection bd = new BlobDetection(color(255,255,255));
  QuadGraph q = new QuadGraph();

  //PImage thresImage = threshold(scharr(thresholdHSB(img,70,160,80,170,80,170)),100);
  //PImage blobImage = bd.findConnectedComponents((thresholdHSB(img,50,170,60,255,60,140)),true);
  
  image(bd.findConnectedComponents(imgt,true), 2*img.width,0);
  
  PImage imgcon = convolute(nim);
  imgSc = scharr(imgcon);
  PImage imgTh = threshold(imgSc,100);
 // image(thresImage,600,0);
  //image(scharr(imgTh), 600,0);
  Hough h=new Hough(scharr(imgt));
  List<PVector> corners =q.findBestQuad(h.hough(threshold(scharr(imgt),100),4),imgTh.width,imgTh.height,imgTh.width*imgTh.height,10,false);
   q.plotQuad(corners);
  h.plot(h.hough(threshold(scharr(imgt),100),4),imgt);
  /*
    //image(thresholdHue(img,(int)(thresholdBar1.getPos()*255),(int)(thresholdBar2.getPos()*255)),600,0);
    //image(threshold(img,(int)(thresholdBar1.getPos()*255)), 0, 0);

    PImage imgt = threshold(img,(int)(thresholdBar1.getPos()*255));
    //image(img,0,0);
    image(imgt,0,0);
  
    
    //println(imagesEqual(imgt,imgTh));
    //image(imgt,600,0);

   */
    hueBar1.display();
    hueBar1.update();
    hueBar2.display();
    hueBar2.update();
    satBar1.display();
    satBar1.update();
    satBar2.display();
    satBar2.update();
    briBar1.display();
    briBar1.update();
    briBar2.display();
    briBar2.update();
    println("hueBar1 : "+ hueBar1.getPos()*255 + " ; hueBar : "+ hueBar2.getPos()*255 +" ; satBar1 : "+ satBar1.getPos()*255 +" ; satBar2 : "+ satBar2.getPos()*255 
      +" ; briBar1 : "+ briBar1.getPos()*255 +" ; briBar2 : "+ briBar2.getPos()*255 );

}
PImage threshold(PImage img, int threshold){
// create a new, initially transparent, 'result' image
  PImage result = createImage(img.width, img.height, RGB);
  result.loadPixels();
  img.loadPixels();
  
  for(int i = 0; i < img.width * img.height; i++) {
    if(brightness(img.pixels[i]) < threshold){
      result.pixels[i] = color(0,0,0);
    }else{
       result.pixels[i]= color(255,255,255);
    }
  }
  result.updatePixels();
  return result;
}
PImage thresholdHSB(PImage img, int minH, int maxH, int minS,int maxS,int minB,int maxB){
  
  PImage result = createImage(img.width, img.height, RGB);
  result.loadPixels();
  img.loadPixels();
    
  for(int i = 0; i < img.width * img.height; i++) {
    if(hue(img.pixels[i]) <= maxH && hue(img.pixels[i]) >= minH && brightness(img.pixels[i]) <= maxB && brightness(img.pixels[i]) >= minB && saturation(img.pixels[i]) <= maxS && saturation(img.pixels[i]) >= minS){
      result.pixels[i] = color(255);
    }else{
      result.pixels[i]= color(0,0,0);
    } 
   
  }
   result.updatePixels();
  return result;
} 

PImage thresholdHue(PImage img, int minH, int maxH){
  
  PImage result = createImage(img.width, img.height, RGB);
  result.loadPixels();
  img.loadPixels();  
  for(int i = 0; i < img.width * img.height; i++) {
    // do something with the pixel img.pixels[i]
    if(hue(img.pixels[i]) < maxH && hue(img.pixels[i]) > minH){
      result.pixels[i] = img.pixels[i];
    }else{
      result.pixels[i]= color(0,0,0);
    }
  }
  result.updatePixels();
  return result;
}

PImage convolute(PImage img) {
  /*float[][] kernel = { { 0, 0, 0 },
  { 0, 2, 0 },
  { 0, 0, 0 }}; */
  /*float[][] kernel = { { 0, 1, 0 },
  { 1, 0, 1 },
  { 0, 1, 0 }}; */
  float[][] kernel = { { 9, 12, 9 },
                       { 12, 15, 12 },
                       { 9, 12, 9 }};
  float normFactor = 99.f;
  PImage result = createImage(img.width, img.height, ALPHA);
  img.loadPixels();
  result.loadPixels();
  int kernelSize=3;
 
  for(int qx=1; qx <img.width-1; ++qx){
    for(int qy = 1; qy<img.height-1;qy++){
      
      int sum=0;
      for(int px =-kernelSize/2; px < + kernelSize/2;px++){
        for(int py = -kernelSize/2; py < kernelSize/2;py++){
          
          sum+=brightness(img.pixels[(qy+py)*img.width+px+qx])*kernel[px+ kernelSize/2][py+kernelSize/2];
           
        }
      } 
      result.pixels[qy*img.width+qx]=(int)(sum/normFactor);
    }   
  }
  result.updatePixels();
  return result;
}

boolean imagesEqual(PImage img1, PImage img2){
  if(img1.width != img2.width || img1.height != img2.height)
    return false;
  for(int i = 0; i < img1.width*img1.height ; i++)
    //assuming that all the three channels have the same value
    if(red(img1.pixels[i]) != red(img2.pixels[i]))
       return false;
  return true;
}

PImage scharr(PImage img) {
  int kernelSize=3;
  float normFactor = 1f;
  float[][] vKernel = {
  { 3, 0, -3 },
  { 10, 0, -10 },
  { 3, 0, -3 } };
  float[][] hKernel = {
  { 3, 10, 3 },
  { 0, 0, 0 },
  { -3, -10, -3 } };
  float max=0;
  float[] buffer = new float[img.width * img.height];
  PImage result = createImage(img.width, img.height, ALPHA);
  
  // clear the image
  for (int i = 0; i < img.width * img.height; i++) {
  result.pixels[i] = color(0);
  }

  
  for(int qx=1; qx <img.width-1; ++qx){
    for(int qy = 1; qy<img.height-1;++qy){
      
      float sum=0;
      float sumV=0;float sumH=0;
      for(int px =-kernelSize/2; px <=  kernelSize/2;px++){
        for(int py = -(kernelSize/2); py <= kernelSize/2;py++){  
          sumV+=brightness(img.pixels[(qy+py)*img.width+px+qx])*vKernel[px+ kernelSize/2][py+kernelSize/2];
           sumH+=brightness(img.pixels[(qy+py)*img.width+px+qx])*hKernel[px+ kernelSize/2][py+kernelSize/2];
        }
      } 
      sum=sqrt(pow((sumV), 2) + pow((sumH), 2));
      if(sum>max){
        max=sum;
      } 
      buffer[qy*img.width+qx]=sum/normFactor;
    }   
  }
   
  for (int y = 1; y < img.height - 1; y++) { // Skip top and bottom edges
    for (int x = 1; x < img.width - 1; x++) { // Skip left and right
      int val=(int) ((buffer[y * img.width + x] / max)*255);
      result.pixels[y * img.width + x]=color(val);
    }
  }
  result.updatePixels();  
  return result;
  }


 