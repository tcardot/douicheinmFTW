import gab.opencv.*;
import processing.video.*;
import java.util.LinkedList;

public ImageProcessing imgproc;

PGraphics gameSurface;
PGraphics backGroundScore;
PGraphics topView;
PGraphics scoreboard;

PGraphics barChart;
HScrollbar hScroll;
boolean mouseOverGame;  //pour différencier le mouvement du jeu et du scroll

float score = 0;  
LinkedList<Float> scores = new LinkedList<Float>();
float lastValueHscroll = 0.5;  


float angleX = 0;
float angleZ = 0;
float deltaX = 0;
float deltaZ = 0;

PVector rot = new PVector();
float varMax = (float)radians(45);
float lastX =0;
float lastZ =0;
float reduceFactor = 0.6f;

float ox = 0;
float oy = 0;

float cx=0;
float cy=0;

float tVel = PI*2/(3*500);


boolean rectangleMode=false;
boolean clicked=false;
Tray tray;
SphereMover sphereMover;
final float G = 9.81;

void settings() {
  size(800, 800, P3D);
 
}
void setup() {
  
  rot.x = 0;
  rot.y = 0;
  rot.z = 0;
  imgproc = new ImageProcessing();
  String []args = {"Image processing window"};
  PApplet.runSketch(args, imgproc);
  
  gameSurface =     createGraphics(width, height-200, P3D);  //here 300 in the document
  backGroundScore = createGraphics(width, height-gameSurface.height, P2D);
  topView =         createGraphics(width/8, width/8, P2D);
  scoreboard =      createGraphics(width/8, width/8, P2D);
  
  barChart = createGraphics(width/2, height/8, P2D);
  hScroll = new HScrollbar(width/3, 700, barChart.width, 15);
  
  
  
  surface.setLocation(0,0);
  surface.setResizable(true);
  gameSurface.noStroke();
  sphereMover = new SphereMover();
  tray = new Tray();
}


void mousePressed() {
  ox = mouseX;
  oy = mouseY;
  if(ox <=gameSurface.width  && oy<=gameSurface.height ){
    mouseOverGame = true;
  }else{
    mouseOverGame = false;
  }
  clicked = true;
}
void mouseDragged() {
  if(mouseOverGame){
    deltaX = tVel*(oy - mouseY);
    deltaZ = tVel*(ox - mouseX);
  }
}
void mouseWheel(MouseEvent event){
  float e = event.getCount();
  tVel -= e/1000;
  if(tVel < 0){
    tVel = PI*2/(3*500);
  }
}
void mouseReleased() {
  clicked = false;
  angleX += deltaX;
  angleZ += deltaZ;
  deltaX = 0;
  deltaZ = 0;
  if (angleX > PI/3) {
    angleX = PI/3;
  }
  if (angleX < -PI/3) {
    angleX = -PI/3;
  }
  if (angleZ > PI/3) {
    angleZ = PI/3;
  }
  if (angleZ < -PI/3) {
    angleZ = -PI/3;
  }
}
void keyPressed() {
  if (key==CODED) {
    if (keyCode==SHIFT) {
      rectangleMode=true;
    }
  }
}
void keyReleased() {
  if (key==CODED) {
    if (keyCode==SHIFT) {
      rectangleMode=false;
    }
  }
}

void draw() {
  
  drawBackGroundScore();
  image(backGroundScore, 0,height-backGroundScore.height);
  
  drawTopView();
  image(topView,50, height -125);
  
  drawScoreBoard();
  image(scoreboard, 150, height -125);
  
  drawGame();
  image(gameSurface, 0,0);
  
  hScroll.update();
  hScroll.display();
  drawBarChart();
  image(barChart, hScroll.xPosition, height-150);
 
}

void drawGame(){
  
  gameSurface.beginDraw();
  
  gameSurface.stroke(255, 255, 255);
  gameSurface.background(200);
  
// gameSurface.camera(0, 0, 550, 0, 0, 0, 0, 1, 0);
  drawAxes();
  
  float rotratio = 0.75f;
  float rX = 0,rZ = 0;
  
  PVector temp = imgproc.getRotation();
  if(temp == null) temp = new PVector(0,0,0);
  //temp.x = rectAngle(temp.x);
  //temp.y = rectAngle(temp.y);
  if( imgproc.isQuad() != null && imgproc.isQuad()){
    rot = temp;
    
  }

  lightsEnv();  //lumières
  
   rX = -rectAngle(rot.x);
   rZ = rectAngle(rot.z);
   
   rX *= rotratio;
   rZ *= rotratio;
   rX = reduceBigVariation(rX,lastX);
   rZ = reduceBigVariation(rZ,lastZ);
   lastX = rX;
   lastZ = rZ;

  if (rectangleMode) {
    
    gameSurface.pushMatrix();
    gameSurface.translate(width/2,height/2);
    gameSurface.rotateX(-PI/2);

    tray.display();
    sphereMover.display();
    if(clicked && mouseOverGame){  
      float x=ox-width/2;
      float z=-height/2+oy;
       clicked = false;
        if(x>=-250 && x<=250 && z>=-250 && z<=250){  
              tray.addCylinder(x,z, sphereMover.getX(), sphereMover.getZ());
        }
            
    }
   
    gameSurface.popMatrix();

  } else {
    gameSurface.pushMatrix();
     gameSurface.translate(width/2,height/2);
     rotate(rX, 0, rZ);
     sphereMover.update(-rX, 0, rZ,tray.getCylinders());

     tray.display();
     sphereMover.display();
    gameSurface.popMatrix();
  }

  gameSurface.textSize(100);
  gameSurface.text(mouseX + " ; " + mouseY, -500, -500);
  gameSurface.endDraw();
}


void drawTopView(){
  topView.beginDraw();
  topView.background(200, 0, 50);
  
  float ratio = topView.width/tray.BOARDWIDTH;  
  
  topView.pushMatrix(); 
  topView.translate(topView.width/2, topView.height/2);
  
  for(int i = 0; i< tray.cylinders.size(); i++){
    PVector v = tray.cylinders.get(i).getLocation();
     topView.fill(0,255,0);
     float cDiam = tray.cylinders.get(0).getRadius()*ratio*2;
     topView.ellipse(v.x*ratio,v.z*ratio,cDiam,cDiam); 
    topView.noStroke();
  }
  
  //sphere
  topView.fill(0,0,255);
  topView.noStroke();
  float sDiam = sphereMover.getRadius() * ratio*2;
  topView.ellipse(sphereMover.location.x*ratio, sphereMover.location.z*ratio, sDiam,sDiam);
  
  topView.popMatrix();
  topView.endDraw();
}

void drawBackGroundScore(){
  backGroundScore.beginDraw();
  backGroundScore.background(150);
  backGroundScore.endDraw();
}

void drawScoreBoard(){ 
   scoreboard.beginDraw();
   
  scoreboard.pushMatrix();
     scoreboard.background(100);
     scoreboard.textSize(13);
     scoreboard.text("Total score : ", 3,15);
     scoreboard.text(score, 10,30);
     scoreboard.text("Nb of cyl: "+tray.cylinders.size() , 3, 45);
     scoreboard.text("Velocity : " + sphereMover.velocity.mag() , 10, 60);
     scoreboard.text("Ratio vel.: " +tVel , 3, 75);
     scoreboard.text("Last score " + (scores.size()!= 0 ? scores.get(scores.size()-1) : 0), 3, 90); 
  scoreboard.popMatrix();
   
   scoreboard.endDraw();
}

void drawBarChart(){ 
  
   float cubeL = barChart.width/50;
   float widthCube = (hScroll.getPos()+0.5)*cubeL;
   
   barChart.beginDraw();
     barChart.pushMatrix();
     
     barChart.fill(50, 150, 150);
     
     if(lastValueHscroll != hScroll.getPos() /*|| cubeL != lastCubeL */){  //s'il y a un changement de dimension de carré on doit tout redessiner
       barChart.fill(150);
       lastValueHscroll = hScroll.getPos();
       barChart.fill(50, 150, 150);
       println(hScroll.getPos());
       
       drawScores(widthCube,cubeL);
       
     }else if(!scores.isEmpty()){  // sinon on ajoute la dernière colonne de carré
       drawOneScore(scores.size()-1, widthCube,cubeL);
       if(scores.size() > 150){   //Si le nombre de scores enregistrés dépasse 150, on réduit la taille de la liste des scores
          for(int i = 0; i< 100; i++)
            scores.remove();
          
          /*barChart.fill(150);
          barChart.fill(50, 150, 150);
          drawScores(widthCube,cubeL);
          */
        }
     }
    
    
       
     hScroll.update();
     hScroll.display();
     barChart.popMatrix();
   barChart.endDraw();
}

void drawScores(float widthCube, float cubeL){
  for(int i = 0; i< scores.size(); i++){
    if(scores.get(i)>=0)
      drawOneScore(i, widthCube, cubeL);
  }
}
void drawOneScore(int index, float widthCube, float cubeL){
  float temp = round(scores.get(index)/cubeL);//hauteur des carré
   while(temp != 0 ){  
     barChart.rect(widthCube*index,barChart.height-temp*cubeL,widthCube,cubeL,2,2,2,2);
     temp-=1;
   }
}
  

void drawAxes() {// référentiel depuis le centre du plateau
  gameSurface.stroke(255, 0, 0);
  gameSurface.pushMatrix();
    gameSurface.translate(width/2, height/2);
    gameSurface.line(0, 0, 0, 1000, 0, 0 );
    gameSurface.stroke(0, 200, 0);
    gameSurface.line(0, 0, 0, 0, -1000, 0 );
    gameSurface.stroke(0, 0, 100);
    gameSurface.line(0, 0, 0, 0, 0, 1000 );
  gameSurface.popMatrix();
}

void lightsEnv() {
  gameSurface.directionalLight(100, 100, 100, 1, 1, 0);
  gameSurface.ambientLight(200, 200, 200);
}
void rotate(float aX, float aY, float aZ) {
  gameSurface.rotateX(aX);
  gameSurface.rotateY(aY);
  gameSurface.rotateZ(aZ);
}
float rectAngle(float a){
  
  if( abs(a) < PI/3){
    return a;
  }
  if(a <= PI && a >= 0){
    return Math.max(a - PI,-PI/3);
  }
  return Math.min(a+ PI, PI/3);
}

float reduceBigVariation(float val, float lastV){
  float diff = (val - lastV) ;
  if(abs(diff) > varMax){
    return val +diff*reduceFactor;
  }
  return val; 
}