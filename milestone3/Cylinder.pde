class Cylinder {
  float cylinderBaseSize = 25;
  float cylinderHeight = 50;
  int cylinderResolution = 40;
  PShape openCylinder = new PShape();
  PShape diskU = new PShape();
  PVector position = new PVector();

  Cylinder(float px, float py, float pz) {
    position.x = px;
    position.y = py;
    position.z = pz;
    float angle;
    float[] x = new float[cylinderResolution ];
    float[] z = new float[cylinderResolution ];

    for (int i = 0; i < x.length; i++) {
      angle = (TWO_PI / cylinderResolution) * i;
      x[i] = sin(angle) * cylinderBaseSize;
      z[i] = cos(angle) * cylinderBaseSize;
    }
    openCylinder = gameSurface.createShape();
    openCylinder.beginShape(QUAD_STRIP);
    //draw the border of the cylinder
    for (int i = 0; i < x.length; i++) {
      openCylinder.vertex(x[i], 0, z[i]);
      openCylinder.vertex(x[i], -cylinderHeight, z[i] );
    }
    openCylinder.vertex(x[0],0,z[0]);
    openCylinder.vertex(x[0], -cylinderHeight,z[0]);
    openCylinder.endShape();
    
    diskU = gameSurface.createShape();
    diskU.beginShape(TRIANGLE_STRIP);
    for (int i = 0; i< x.length; i++) {
      diskU.vertex(x[i], -cylinderHeight, z[i] );
      diskU.vertex(0, -cylinderHeight, 0);
    }
     diskU.vertex(x[0], -cylinderHeight, z[0] );
     diskU.vertex(0, -cylinderHeight, z[0]);
    diskU.endShape();
  }
  void display() {
    gameSurface.pushMatrix();
    gameSurface.translate(position.x, position.y, position.z);
    gameSurface.noStroke();
   gameSurface.fill(231,235,128);
    gameSurface.shape(openCylinder);
    gameSurface.shape(diskU);
    gameSurface.popMatrix();
  }
  PVector getLocation() {
    return new PVector(position.x, position.y, position.z);
  }
  float getRadius() {
    return cylinderBaseSize;
  }
}