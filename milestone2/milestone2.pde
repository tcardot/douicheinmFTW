
PImage img;

void settings() {
  size(1200, 300);
}

void setup() {

  img = loadImage("board1.jpg");
  img.resize(400,300);
}
 
void draw() {
  
   BlobDetection bd = new BlobDetection(color(255,255,255));
   QuadGraph q = new QuadGraph();
   
  PImage blobbed = bd.findConnectedComponents(thresholdHSB(img,91,139,80,255,25,180),true);
  PImage edgeDetected = threshold(scharr(convolute(thresholdHSB(img,91,139,80,255,25,180))),100);
  PImage ascended = threshold( scharr(convolute(blobbed)),100);
  Hough hough = new Hough(ascended); 
  
   image(img,0,0);
   image(edgeDetected,400,0);
   image(blobbed,800,0);
   
   hough.plot(hough.hough(4),img);
   q.plotQuad(q.findBestQuad(hough.hough(4),img.width,img.height,img.width*img.height,10,false));
}
PImage threshold(PImage img, int threshold){
// create a new, initially transparent, 'result' image
  PImage result = createImage(img.width, img.height, RGB);
  result.loadPixels();
  img.loadPixels();
  
  for(int i = 0; i < img.width * img.height; i++) {
    if(brightness(img.pixels[i]) < threshold){
      result.pixels[i] = color(0,0,0);
    }else{
       result.pixels[i]= color(255,255,255);
    }
  }
  result.updatePixels();
  return result;
}
PImage thresholdHSB(PImage img, int minH, int maxH, int minS,int maxS,int minB,int maxB){
  
  PImage result = createImage(img.width, img.height, RGB);
  result.loadPixels();
  img.loadPixels();
    
  for(int i = 0; i < img.width * img.height; i++) {
    if(hue(img.pixels[i]) <= maxH && hue(img.pixels[i]) >= minH && brightness(img.pixels[i]) <= maxB && brightness(img.pixels[i]) >= minB && saturation(img.pixels[i]) <= maxS && saturation(img.pixels[i]) >= minS){
      result.pixels[i] = color(255);
    }else{
      result.pixels[i]= color(0,0,0);
    } 
   
  }
   result.updatePixels();
  return result;
} 

PImage thresholdHue(PImage img, int minH, int maxH){
  
  PImage result = createImage(img.width, img.height, RGB);
  result.loadPixels();
  img.loadPixels();  
  for(int i = 0; i < img.width * img.height; i++) {
    // do something with the pixel img.pixels[i]
    if(hue(img.pixels[i]) < maxH && hue(img.pixels[i]) > minH){
      result.pixels[i] = img.pixels[i];
    }else{
      result.pixels[i]= color(0,0,0);
    }
  }
  result.updatePixels();
  return result;
}

PImage convolute(PImage img) {
  /*float[][] kernel = { { 0, 0, 0 },
  { 0, 2, 0 },
  { 0, 0, 0 }}; */
  /*float[][] kernel = { { 0, 1, 0 },
  { 1, 0, 1 },
  { 0, 1, 0 }}; */
  float[][] kernel = { { 9, 12, 9 },
                       { 12, 15, 12 },
                       { 9, 12, 9 }};
  float normFactor = 99.f;
  PImage result = createImage(img.width, img.height, ALPHA);
  img.loadPixels();
  result.loadPixels();
  int kernelSize=3;
 
  for(int qx=1; qx <img.width-1; ++qx){
    for(int qy = 1; qy<img.height-1;qy++){
      
      int sum=0;
      for(int px =-kernelSize/2; px < + kernelSize/2;px++){
        for(int py = -kernelSize/2; py < kernelSize/2;py++){
          
          sum+=brightness(img.pixels[(qy+py)*img.width+px+qx])*kernel[px+ kernelSize/2][py+kernelSize/2];
           
        }
      } 
      result.pixels[qy*img.width+qx]=(int)(sum/normFactor);
    }   
  }
  result.updatePixels();
  return result;
}

boolean imagesEqual(PImage img1, PImage img2){
  if(img1.width != img2.width || img1.height != img2.height)
    return false;
  for(int i = 0; i < img1.width*img1.height ; i++)
    //assuming that all the three channels have the same value
    if(red(img1.pixels[i]) != red(img2.pixels[i]))
       return false;
  return true;
}

PImage scharr(PImage img) {
  int kernelSize=3;
  float normFactor = 1f;
  float[][] vKernel = {
  { 3, 0, -3 },
  { 10, 0, -10 },
  { 3, 0, -3 } };
  float[][] hKernel = {
  { 3, 10, 3 },
  { 0, 0, 0 },
  { -3, -10, -3 } };
  float max=0;
  float[] buffer = new float[img.width * img.height];
  PImage result = createImage(img.width, img.height, ALPHA);
  
  // clear the image
  for (int i = 0; i < img.width * img.height; i++) {
  result.pixels[i] = color(0);
  }

  
  for(int qx=1; qx <img.width-1; ++qx){
    for(int qy = 1; qy<img.height-1;++qy){
      
      float sum=0;
      float sumV=0;float sumH=0;
      for(int px =-kernelSize/2; px <=  kernelSize/2;px++){
        for(int py = -(kernelSize/2); py <= kernelSize/2;py++){  
          sumV+=brightness(img.pixels[(qy+py)*img.width+px+qx])*vKernel[px+ kernelSize/2][py+kernelSize/2];
           sumH+=brightness(img.pixels[(qy+py)*img.width+px+qx])*hKernel[px+ kernelSize/2][py+kernelSize/2];
        }
      } 
      sum=sqrt(pow((sumV), 2) + pow((sumH), 2));
      if(sum>max){
        max=sum;
      } 
      buffer[qy*img.width+qx]=sum/normFactor;
    }   
  }
   
  for (int y = 1; y < img.height - 1; y++) { // Skip top and bottom edges
    for (int x = 1; x < img.width - 1; x++) { // Skip left and right
      int val=(int) ((buffer[y * img.width + x] / max)*255);
      result.pixels[y * img.width + x]=color(val);
    }
  }
  result.updatePixels();  
  return result;
  }


 