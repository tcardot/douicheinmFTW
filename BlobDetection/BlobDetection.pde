import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.Iterator;
PImage im; 

//Test for BlobDetection
/*void settings(){
  size(600,600);
}

void setup(){
  im = loadImage("week9-BlobDetection_Test.png") ;
  PImage result = createImage(im.width, im.height, RGB);
  result = BlobDetection.findConnectedComponents(im, false);
  result = BlobDetection.findConnectedComponents(im, true);
  result.loadPixels();
  image(im, 200,0);
  image(result,0,0);
}*/

//OUR ORDER OF NEIGHBOURS : LEFT, TOP LEFT, TOP CENTRE, TOP RIGHT
class BlobDetection {
  //the color we want to detect
  color toDetect;  
  
  BlobDetection(color col){
    this.toDetect = col;
  }
  
  PImage findConnectedComponents(PImage input, boolean onlyBiggest){
     input.loadPixels();
     PImage result = createImage(input.width, input.height, RGB);
     result.loadPixels();
     
    // First pass: label the pixels and store labels' equivalences
    
    int [] labels= new int [input.width*input.height];
    List<TreeSet<Integer>> labelsEquivalences= new ArrayList<TreeSet<Integer>>();
    int currentLabel=1;
    //when label = 0, that means the pixel hasn't the right color
    
    for(int i = 0; i < input.width*input.height ; i++){
      
      //si la classe d'équivalence correspondant au currentLabel n'existe pas encore, on la crée
      if(labelsEquivalences.size() < currentLabel){
        TreeSet<Integer> tree = new TreeSet<Integer> ();
        tree.add(currentLabel);
        labelsEquivalences.add(tree);
      }
      
      if(input.pixels[i] == toDetect){  //the color we want to detect
        //labels of the neighboursh
        int neighbours[] = getNeighbours(i, labels, input.width, input.height);
        
        if(checkNeighbours(neighbours)){  //if at least one of neighbours is labelled 
          
          if(allNeighLabelledSame(neighbours)){
            labels[i] = neighbours[0];  //as all labels are the same
            
          }else{ //took the smallest non zero label and add equivalents 
          
           int minLabel = Integer.MAX_VALUE; //will always be smaller, as there is a neighbour labelled
           for(int k = 0; k < neighbours.length; k++){//not zero, since zero = not labelled
             if(neighbours[k] != 0 && neighbours[k] < minLabel)  
               minLabel = neighbours[k];
           }
          
          labels[i] = minLabel;  //attribution du smallest label au current pixel
           
           for(int k = 0; k < neighbours.length; k++){//adding the equivalence values to the labelled neighbour
             //println("k nei : " + neighbours[k]);
             if(neighbours[k]!= 0 && neighbours[k] != minLabel)
               labelsEquivalences.get(neighbours[k]-1).add(minLabel); //In labelsEquivalences : label 1 corresponds to 0,label 2 -> 1, hence the -1
               //for first pixel won't come here, neighbour[k] gives us the label
           }
          }
          
        }else{  //if no neighbour labelled, but is the color toDetect
          labels[i] = currentLabel;
          currentLabel++;
          //especially for the last pixel of the image
           if(labelsEquivalences.size() < currentLabel){
            TreeSet<Integer> tree = new TreeSet<Integer> ();
            tree.add(currentLabel);
            labelsEquivalences.add(tree);
          }
        }
        
      }else{  //not the color toDetect
       labels[i] = 0;  
      }
    }
    
    //println(" Labelisation finished");
    
    //====================// Union of equivalences
    for(int g = 0; g < labelsEquivalences.size(); g++){
      Integer minLabel = labelsEquivalences.get(g).first();
      Iterator<Integer> it = labelsEquivalences.get(g).iterator();
      while(it.hasNext()){
        labelsEquivalences.get(it.next()-1).add(minLabel);
      }
    } 
    //====================//
      
    
    // TODO!
    // Second pass: re-label the pixels by their equivalent class
    int [] nbforLabels = new int[labelsEquivalences.size()];  //at most labelsEquivalences.size() labels
    for(int j = 0; j > nbforLabels.length; j++)
      nbforLabels[j] = 0;
      
    for(int i = 0; i < input.width*input.height; i++){
      if(labels[i] == 0) continue;  //we don't count the number of non white pixels
       labels[i] = labelsEquivalences.get(labels[i]-1).first(); //index of equivalence start at 0 with the tree(1) 
       if(onlyBiggest){
         nbforLabels[labels[i]-1]+= 1;  //equivalence,    MODIFIED HERE -1, we don't count the number of pixel of label 0 so we start to count for label 1, thus  nbforLabels[2] = number of pixel of label 3
       }
    }
    // if onlyBiggest==true, count the number of pixels for each label
      
    // TODO!
    // Finally,
    // if onlyBiggest==false, output an image with each blob colored in one uniform color
    // if onlyBiggest==true, output an image with the biggest blob colored in white and the others in black
    // TODO!
    
    //le tableau de couleurs random
    //assign one color to each possible label
    int col[] = new int[labelsEquivalences.size()];
    for(int j = 0; j< col.length; j++){
      col[j] = /*color(255);*/color(random(255),random(255),random(255));
    }
    
    if(!onlyBiggest){  //if we want multiple blobs
      for(int i = 0; i < input.width*input.height; i++){
        if(labels[i] != 0){
          result.pixels[i] = col[labels[i]];//on applique la couleur du blob
        }else{
          result.pixels[i] = input.pixels[i];//on laisse la couleur originale
        }
      }
    }else{
       //index of label that contains the most pixels
      int max = 0; int maxLabel = -1; 
      for(int i = 0; i < nbforLabels.length; i++){
         if(nbforLabels[i] > max){
           max =  nbforLabels[i];
           maxLabel = i;
         }
      }
      //color only the blob that have the most pixels
      for(int i = 0; i < input.width *input.height; i++){
        if(labels[i]-1  == maxLabel) result.pixels[i] = color(255);//modified here, -1 since the start label is 1, see nbforLabels
        else result.pixels[i] = color(0);
      }
    }
    
    result.updatePixels();
    return result;
   }
   
   //==============================================//
   //Helping functions
   
   //return the array of the label of the neighbors of i
   //labels: array of labels currently being filled
   int[] getNeighbours(int i, int labels[], int widthImage, int heightImage){
       //order : left, left top, center top, right top
      int tab[] = new int[4];
      //Initialisation of the table, all labels are 0
      for(int j = 0; j < tab.length;j++)
        tab[j] = 0;
      
      //get the coordinations of i, in order to make the tests easier 
      int y = i/widthImage;
      int x = i%widthImage;
      
      if(x == 0){
        if(y == 0){
          return tab;
        }else /*if(y== heightImage-1)*/{
          tab[2] = labels[i-widthImage]; tab[3] =  labels[i -widthImage +1] ;
        }
       }else if(x == widthImage-1){ //right border
          if(y == 0){
            tab[0] = labels[i-1];
          }else {
              tab[0] = labels[i-1]; tab[1] = labels[i -widthImage -1]; tab[2] = labels[i-widthImage]; 
          }
       }else if(y==0){
         tab[0] = labels[i-1];
       }else{
         //When not in borders
       tab[0] = labels[i-1]; tab[1] = labels[i -widthImage -1]; tab[2] = labels[i-widthImage]; tab[3] = labels[i-widthImage+1];
       }  
      return tab;
      
   }
   //return true if there is at least one neighbour labelled
   boolean checkNeighbours(int[] neighbours){
     for(int i = 0; i< neighbours.length; i++)
       if(neighbours[i] != 0) return true;
     return false;
   }
   
   //all the one labelled are the same or all have the same label ???? right now it's all same, it seems to work
   boolean allNeighLabelledSame(int[] neighbours){
     /*int val = neighbours[0];  //reference value
     for(int i = 1; i< neighbours.length; i++)
       if(neighbours[i]!= val)
         return false;
         */
         int val = 0;
      for(int i = 0; i< neighbours.length; i++){
        if(neighbours[i] != 0)
          val = neighbours[i];
      }   
       for(int i = 0; i< neighbours.length; i++){
        if(neighbours[i] != 0 && neighbours[i] != val)
          return false;
       } 
     return true;
   }
   
   //return the index of the pixel corresponding to the kth neighbour, with respects to the currentIndex
   int indexLabel(int k, int currentIndex, int widthImage){
     return k == 0 ? currentIndex-1 : currentIndex-widthImage-1+ k;
     //k == 0 : the neighbour left to the current one
   }
   
}

//Test for neighbours that are labelled
/*if(x == 0){
        if(y == 0){
          return false;
        }else if(y== heightImage-1){
          
          return (labels[i-widthImage] != 0 || labels[i -widthImage +1] != 0) ;
        }
      }else if(x == widthImage-1){
          if(y == 0){
            return labels[i-1] != 0;
          }else {
              return (labels[i-1] != 0 || labels[i-widthImage] != 0 || labels[i -widthImage -1] != 0);
            }
     }else if(y==0) return labels[i-1] != 0;
     
     return (labels[i-1] != 0 || labels[i-widthImage] != 0 || labels[i -widthImage -1] != 0|| labels[i -widthImage +1] != 0);
*/