float pox = 0;
float poy = 0;
My3DPoint eye = new My3DPoint(0, 0, -5000);
My3DPoint origin = new My3DPoint(0, 0, 0);
My3DBox input3DBox = new My3DBox(origin, 100, 150, 300);

void settings() {
  size(1000, 1000, P2D);
}
float[] center(My3DBox box){
  float px = 0;
  float py = 0;
  float pz = 0;
  for(int i = 0; i < box.p.length;++i){
    px += box.p[i].x/box.p.length;
    py += box.p[i].y/box.p.length;
    pz += box.p[i].z/box.p.length;
  }
  float[] t = {px,py,pz};
  return t;
}
void mouseDragged() 
{


  float[] pos = center(input3DBox);
  float[][] transM = translationMatrix(-pos[0], -pos[1], -pos[2]);
  input3DBox = transformBox(input3DBox, transM);
  float[][] scaleM = scaleMatrix(1+(poy-mouseY)/1000,1+ (poy-mouseY)/1000,1+ (poy-mouseY)/1000);
  input3DBox = transformBox(input3DBox, scaleM);
  float[][] transM2 = translationMatrix(pos[0], pos[1], pos[2]);
  input3DBox = transformBox(input3DBox, transM2);
  
}
void mousePressed(){
  pox = mouseX;
  poy = mouseY;
}
void keyPressed() {
  float[] pos = center(input3DBox);
 if (key == CODED) {
    if (keyCode == UP) {
      float[][] transM = translationMatrix(-pos[0], -pos[1], -pos[2]);
      input3DBox = transformBox(input3DBox, transM);
      float[][] rotM = rotateXMatrix(PI/16);
      input3DBox = transformBox(input3DBox, rotM);
      float[][] transM2 = translationMatrix(pos[0], pos[1], pos[2]);
      input3DBox = transformBox(input3DBox, transM2);
    } 
    if (keyCode == DOWN) {
      float[][] transM = translationMatrix(-pos[0], -pos[1], -pos[2]);
      input3DBox = transformBox(input3DBox, transM);
      float[][] rotM = rotateYMatrix(PI/16);
      input3DBox = transformBox(input3DBox, rotM);
      float[][] transM2 = translationMatrix(pos[0], pos[1], pos[2]);
      input3DBox = transformBox(input3DBox, transM2);
    } 
  } else {
  }
}
void setup() {
//rotated around x
float[][] transform1 = rotateXMatrix(PI/8);
input3DBox = transformBox(input3DBox, transform1);
//rotated and translated
float[][] transform2 = translationMatrix(200, 200, 0);
input3DBox = transformBox(input3DBox, transform2);
//rotated, translated, and scaled
float[][] transform3 = scaleMatrix(1.2, 1.2, 1.2);
input3DBox = transformBox(input3DBox, transform3);

}
void draw() {
background(255, 255, 255);
projectBox(eye, input3DBox).render();
}


class My3DPoint {
  float x;
  float y;
  float z;
  My3DPoint(float x, float y, float z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }
}
class My2DPoint {
  float x;
  float y;
  My2DPoint(float x, float y) {
    this.x = x;
    this.y = y;
  }
}
My2DPoint projectPoint(My3DPoint eye, My3DPoint p) {
  float px = p.x;
  float py = p.y;
  float pz = p.z;
  px = px - eye.x;
  py = py - eye.y;
  pz = pz - eye.z;
  px = -eye.z * px/(p.z -eye.z);
  py = -eye.z *py/(p.z - eye.z);
  return new My2DPoint(px,py);
}
class My2DBox {
  My2DPoint[] s;
  My2DBox(My2DPoint[] s) {
  this.s = s;
}
  void render(){
    // Complete the code! use only line(x1, y1, x2, y2) built-in function.
    line(s[0].x,s[0].y,s[1].x,s[1].y);
    line(s[0].x,s[0].y,s[3].x,s[3].y);
    line(s[1].x,s[1].y,s[2].x,s[2].y);
    line(s[2].x,s[2].y,s[3].x,s[3].y);
    
    line(s[0].x,s[0].y,s[4].x,s[4].y);
    line(s[1].x,s[1].y,s[5].x,s[5].y);
    line(s[2].x,s[2].y,s[6].x,s[6].y);
    line(s[3].x,s[3].y,s[7].x,s[7].y);
    
    line(s[4].x,s[4].y,s[7].x,s[7].y);
    line(s[4].x,s[4].y,s[5].x,s[5].y);
    line(s[7].x,s[7].y,s[6].x,s[6].y);
    line(s[5].x,s[5].y,s[6].x,s[6].y);
  }
}
class My3DBox {
  My3DPoint[] p;
  My3DBox(My3DPoint origin, float dimX, float dimY, float dimZ){
  float x = origin.x;
  float y = origin.y;
  float z = origin.z;
  this.p = new My3DPoint[]{new My3DPoint(x,y+dimY,z+dimZ),
      new My3DPoint(x,y,z+dimZ),
      new My3DPoint(x+dimX,y,z+dimZ),
      new My3DPoint(x+dimX,y+dimY,z+dimZ),
      new My3DPoint(x,y+dimY,z),
      origin,
      new My3DPoint(x+dimX,y,z),
      new My3DPoint(x+dimX,y+dimY,z)
      };
  }
  My3DBox(My3DPoint[] p) {
  this.p = p;
  }
}
My2DBox projectBox (My3DPoint eye, My3DBox box) {
// Complete the code!
  My2DPoint[] a = new My2DPoint[8];
  for(int i = 0; i< 8; ++i){
    a[i] = projectPoint(eye,box.p[i]); 
  }
  return new My2DBox(a);
}

float[] homogeneous3DPoint (My3DPoint p) {
  float[] result = {p.x, p.y, p.z , 1};
  return result;
}
float[][] rotateXMatrix(float angle) {
  return(new float[][] {{1, 0 , 0 , 0},
  {0, cos(angle), sin(angle) , 0},
  {0, -sin(angle) , cos(angle) , 0},
  {0, 0 , 0 , 1}});
}
float[][] rotateYMatrix(float angle) {
  return(new float[][] {{cos(angle),0 , sin(angle) , 0},
                        {0, 1, 0 , 0},
                        {-sin(angle),0 , cos(angle) , 0},
                        {0, 0 , 0 , 1}});
}
float[][] rotateZMatrix(float angle) {
  return(new float[][] {{cos(angle), -sin(angle) , 0 , 0},
                        {sin(angle), cos(angle), 0 , 0},
                        {0,0 , 1 , 0},
                        {0, 0 , 0 , 1}});
  
}
float[][] scaleMatrix(float x, float y, float z) {
return(new float[][]   {{x, 0 , 0 , 0},
                        {0, y, 0 , 0},
                        {0,0 , z , 0},
                        {0, 0 , 0 , 1}});
}
float[][] translationMatrix(float x, float y, float z) {
return(new float[][]   {{1, 0 , 0 ,x},
                        {0, 1, 0 , y},
                        {0,0 , 1 , z},
                        {0, 0 , 0, 1}});
}
float[] matrixProduct(float[][] a, float[] b) {
  float[] c = new float[a.length];
  for(int i = 0; i < a.length; ++i){
    for(int j = 0; j < a[0].length; ++j){
      c[i] += b[j]*a[i][j];
    }
  }
  return c;
}
My3DPoint euclidian3DPoint (float[] a) {
My3DPoint result = new My3DPoint(a[0]/a[3], a[1]/a[3], a[2]/a[3]);
return result;
}

My3DBox transformBox(My3DBox box, float[][] transformMatrix) {
My3DPoint[] points = new My3DPoint[box.p.length];
for(int i = 0; i< box.p.length; ++i){
  points[i] = euclidian3DPoint(matrixProduct(transformMatrix,homogeneous3DPoint((box.p[i]))));
}
return new My3DBox(points);
}