class Tray{
  final int HEIGHT = -25;
  final float BOARDWIDTH = 500;
  float angleX;
  float angleY;
  float angleZ;
  PImage img;
  private ArrayList<Cylinder> cylinders = new ArrayList();
 Tray(){
   angleX = 0;
   angleY = 0;
   angleZ = 0;
     img = loadImage("surprise.jpg");
     img.resize(500,500);  //added
 
 }
 void rotate(float aX,float aY, float aZ){
    angleX = aX;
    angleY = aY;
    angleZ = aZ;
 }
 void display(){
   
    gameSurface.stroke(255,255,255);
    gameSurface.specular(0, 100, 100);
    gameSurface.fill(50,75,200);
    //shininess(1.0);
   
    gameSurface.box(500,50,500);
    //noStroke();
    gameSurface.fill(255);
    
    gameSurface.beginShape(QUAD_STRIP);
      gameSurface.texture(img);
      gameSurface.vertex(-250,-25,-250,0,0);
      gameSurface.vertex(-250,-25,250,0,456);
      gameSurface.vertex(250,-25,-250,456,0);
      gameSurface.vertex(250,-25,250,456,456);
     gameSurface.endShape();
    gameSurface.fill(200,40,150);
    
    gameSurface.stroke(255);
    for(int i=0;i<cylinders.size();i++){
      cylinders.get(i).display();
    }
 }
 void addCylinder(float x, float z, float sphereX, float sphereZ){
   Cylinder c = new Cylinder(x, HEIGHT, z);
   float distance = sqrt((x-sphereX)*(x-sphereX) +(z-sphereZ)*(z-sphereZ));
   // distance between the sphere and the cylinder
   if(distance >= c.cylinderBaseSize + sphereMover.S_RADIUS){
     cylinders.add(c);
   }
 }
 ArrayList getCylinders(){
   return new ArrayList(cylinders);
 }

}