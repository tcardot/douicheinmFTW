class SphereMover{
  PVector location;
  PVector velocity;
  PVector gravityForce;
  final float G = 0.2;
  final float TRAYW = 500;
  final float TRAYL = 500;
  final float BOUNCE = 0.7;
  final float S_RADIUS = 25;
  
  PImage imgSphere;
  PShape globe;
  
  SphereMover(){
    location = new PVector(100,-50,0);
    velocity = new PVector(0,0,0);
    gravityForce = new PVector(0,0,0);
    
    //added those lines for printing moon
    imgSphere = loadImage("moon.jpg");
    globe = createShape(SPHERE, S_RADIUS);
    globe.setStroke(false);
    globe.setTexture(imgSphere);
  }
  
  void update(float aX, float aY, float aZ, ArrayList<Cylinder> cylinders){
    checkBounds();
    checkCylindersCollision(cylinders);
    
    
    gravityForce.x = sin(aZ) * G;
    gravityForce.z = sin(aX) * G;
    float normalForce = 1;
    float mu = 0.03;
    float frictionMagnitude = normalForce * mu;
    PVector friction = velocity.copy();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);
    
    if(location.y > -50){
      gravityForce.y = 0;
      location.y = -50;
    }
    
    //added those lines to make pluto rotate
    globe.rotateX(friction.z -velocity.z/10);
    //globe.rotateY(-friction.y + velocity.y/200);
    globe.rotateZ(-friction.x);
    //
    
    velocity.add(gravityForce);
    velocity.add(friction);
    location.add(velocity);
  }
  void checkBounds(){
    if(abs(location.x) > TRAYW/2){
      velocity.x *= -1;
      velocity.mult(BOUNCE);
      location.x = (location.x/abs(location.x)) *TRAYW/2;
      //add score
      score-= velocity.mag();
      addScore();
    }
    if(abs(location.z) > TRAYL/2){
      velocity.z *= -1;
      velocity.mult(BOUNCE);
      location.z = (location.z/abs(location.z)) *TRAYL/2;
      //add score
      score-= velocity.mag();
      addScore();
    }
  }
/*  void checkCylindersCollision(ArrayList<Cylinder> cyl){
   cyl.stream().forEach((c)  -> {
     if(Math.sqrt(Math.pow(c.x - location.x,2) + Math.pow(c.y -location.y, 2)) <= (sphere_Radius + c.cylinderBaseSize)){
       PVector normal = (new PVector(c.position.x - location.x, 0, c.position.z -location.z)).normalize();
       velocity = velocity.sub(velocity.dot(normal).mult(2));   
     } 
    }
   );
  }  */
  
  void checkCylindersCollision(ArrayList<Cylinder> cyls){
    for(Cylinder c : cyls){
      checkCylinder(c);
    }
  }
  void checkCylinder(Cylinder cyl){
    PVector nloc = new PVector(location.x,0,location.z);
    PVector nvel = new PVector(velocity.x,0,velocity.z);
    PVector dist = nloc.sub(cyl.getLocation());
    dist.y = 0;
    
    if(dist.mag() <= S_RADIUS + cyl.getRadius()){
      location = cyl.getLocation().add(dist.normalize().mult(cyl.getRadius()+ S_RADIUS));
      location.y = 50;
      PVector v2 = nvel.sub(dist.normalize().mult(2*nvel.dot(dist.normalize())));
      velocity = new PVector(v2.x,v2.y,v2.z);
      
      // add score
      score+= velocity.mag();
      addScore();
    }

}
  void display(){
   gameSurface.pushMatrix();
     
    gameSurface.noStroke();
    gameSurface.fill(231,235,128);
    gameSurface.specular(200);
    gameSurface.translate(location.x,location.y,location.z);
    
    //added
    gameSurface.directionalLight(0, 255, 0, 0, 1, 0);
    gameSurface.shape(globe);  
    //gameSurface.sphere(S_RADIUS);  //removed
   gameSurface.popMatrix();
  }
  float getX(){
    return location.x;
  }
  float getY(){
    return location.y;
  }
  float getZ(){
    return location.z;
  }
  public float getRadius(){
    return S_RADIUS;
  }
  
  //Add the score to the ArrayList of scores
  void addScore(){
    if(!scores.isEmpty()){
        scores.add( score- scores.get(scores.size()-1));  //différence de last score et current
      }else{
        scores.add(score);
     }
  }
}